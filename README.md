# OrabackupCalife

A simple ruby gem.
This command line tool allow you to perform backup/restore for the given database sid provided in input.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'orabackup_calife'
```

And then execute:

    $ bundle exec bin/backup

Or install it yourself as:

    $ gem install orabackup_calife

## Usage

$orabackup --help

## Development

<!-- After checking out  the repo, run `bin/setup`  to install dependencies. Then,  run `rake false` to  run the tests. You  can also run -->
<!-- `bin/console` for an interactive prompt that will allow you to experiment. -->

<!-- To install this gem onto your local machine, run `bundle exec  rake install`. To release a new version, update the version number in -->
<!-- `version.rb`, and then run `bundle exec  rake release`, which will create a git tag for the version,  push git commits and tags, and -->
<!-- push the `.gem` file to [rubygems.org](https://rubygems.org). -->

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/calife/orabackup_calife

