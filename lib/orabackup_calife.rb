require "orabackup_calife/version"
require 'highline/import' # for ask
require 'open3'
# require 'sambal'

module OrabackupCalife


  class Utils
    
    def self.askConfirm(msg)
      
      while true
        printf "#{msg}"
        answer=STDIN.gets.chomp
        return true  if answer =~ /^y/i
        return false if answer =~ /^n/i
      end
      
    end

    def self.ask_username(prompt='')
      username = ask("#{prompt}")
      return username
    end

    def self.ask_password(prompt='')
      pass = ask("#{prompt}") { |q| q.echo = '*' }
      return pass
    end
    
  end
  
=begin
    Command Logic
=end  
  class Command

    @catalog=nil
    
    def initialize(tns)
      @catalog=tns
    end

    # def test
    #   client = Sambal::Client.new(
    #     domain: 'WORKGROUP',
    #     host: '127.0.0.1',
    #     share: '',
    #     user: 'guest',
    #     password: '--no-pass',
    #     port: 445
    #   )
      
    #   client.ls # returns hash of files
    # end

    def check_parameter?(operation,options,args)
      
      case operation
      when "backup"
        
        if args.empty?
          puts "Database name required."
          return false
        end        
        if options[:full] && !options[:schema].nil?
          puts  "Please choose one kind of backup. You can't specify both full (--full) and (--schema) backup."
          return false
        end
        if !options[:full] && options[:schema].nil?
          puts  "Please choose one kind of backup, full (--full) or by owner --schema)."
          return false
        end        
        
        return true
        
        # TODO: implement restore check parameters
      when "restore"
        return true
      end
    end

=begin
    List databases, filter by dbname
    TODO: mettere in OR le condizioni di dbname
=end    
    def list(dbname=array.new)

      @catalog=open_catalog
      lines=@catalog.each_line.to_a

      unless dbname.empty? # filter condition
        pattern_match=%r{.*#{dbname[0]}.*=.*$}i

        counter=0

        row_matching=[]     
        lines.each { |line|
          row_matching << counter if line =~ pattern_match
          counter+=1
        }

        extract_entry(lines,row_matching)
        
      else # No filter condition

        counter=0

        row_matching=[]     
        lines.each { |line|
          counter+=1
          printf "%5s %s" , "#{counter}" , "#{line}"
        }
                
      end

      close_catalog
    end    

=begin
         Backup database
         TODO: mettere in OR le condizioni di dbname
=end    
    def backup(options,args)

      dbname=args[0]
      full=options[:full]

      if options[:user].nil?
        user= OrabackupCalife::Utils.ask_username("Enter username: ") 
      else
        user=options[:user]
      end      

      if options[:password].nil?
        password= OrabackupCalife::Utils.ask_password("Enter password for user: ") 
      else
        password=options[:password]
      end

      schema=options[:schema]

      backup_id="#{dbname}-#{Time.now.strftime("%Y%m%dT%H%M")}"

      file=File.join(options[:output],"#{backup_id}.dmp")
      log=File.join(options[:output],"#{backup_id}.log")

      if full
        cmd_line=%Q{exp #{user}/#{password}@#{dbname} full=y file=#{file} log=#{log} statistics=none }
      else
        cmd_line=%Q{exp #{user}/#{password}@#{dbname} owner=#{schema} file=#{file} log=#{log} statistics=none }
      end

      puts "Running " << cmd_line
      
      stdout, stderr, s= Open3.capture3(cmd_line)

      puts "#{stdout}"
      puts "#{stderr}"
      
      if s.success?
        puts "Backup complete successfully. Dump file=#{file} , log=#{log}"
        return 0
      else
        puts "Error "
        return 1
      end
      
    end
    

=begin
         Restore database
=end
    def restore(dbname,input)
      puts "Here"      
    end

private
    def open_catalog
      begin
        File.open(@catalog)
      rescue Exception => ex
        exit_now!(ex.message)
      end
    end
    
    def close_catalog
      unless @catalog.closed?
        @catalog.close
      end
    end
    
    def extract_entry(all_lines,row_matching)

      row_matching.each do |idx|
        
        printf "%s\n" , "."*3
        
        opened_brackets=0
        closed_brackets=0        
        
        loop do
          
          line=all_lines[idx]
          op_par=line.scan( %r{\(}  ).size
          cl_par=line.scan( %r{\)}  ).size
          
          opened_brackets+=op_par
          closed_brackets+=cl_par
          
          printf "%5s %s" , "#{idx}" , "#{line}"
          
          idx+=1
          
          break if opened_brackets>0 && opened_brackets==closed_brackets
        end
      end

    end
    
  end
  
end
